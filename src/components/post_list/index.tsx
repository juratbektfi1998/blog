import React from "react";
import Link from "next/link";
import { ListGroup, ListGroupItem, Container, Row, Col } from "reactstrap";
import { Transition, CSSTransition } from "react-transition-group";
import IPostsType from "types/posts";

import "./post_list.module.scss";

interface IPostListProps {
    posts: IPostsType;
    isFade?: boolean;
}

const PostList: React.FC<IPostListProps> = (props) => {
    return (
        <Row>
            <Col>
                <ListGroup>
                    {props.posts.map((post, index) => (
                        <CSSTransition
                            in={props.isFade}
                            key={index}
                            timeout={index * 40}
                            classNames="post-list-item"
                        >
                            <ListGroupItem key={index}>
                                <Link href="/posts/:id" as={`/posts/${post.id}`}>
                                    {post.title}
                                </Link>
                            </ListGroupItem>
                        </CSSTransition>
                    ))}
                </ListGroup>
            </Col>
        </Row>
    );
};

export default PostList;
