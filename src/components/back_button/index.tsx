import React from 'react'
import { Row, Col } from 'reactstrap';
import { useRouter } from 'next/router';
import Button from 'src/components/button';

type sizes = 'xs' | 'md' | 'lg' | 'xl';



type ObjType = {
    [key in sizes]: string
}


interface IBackButton {
    size?: Partial<ObjType>
}

const BackButton: React.FC<IBackButton> = (props) => {

    const router = useRouter();

    return (
        <Col {...props} onClick={() => router.back()}>
            <Button text='Back' />
        </Col>
    )
}

export default BackButton
