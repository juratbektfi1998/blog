import { useState } from "react";
import Link from "next/link";
import {
  Collapse,
  Navbar as ReactStrapNavbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";

import "./navbar.module.scss";

export type TNavbarProps = {
  color?: string;
};

const Navbar = ({ color = "", ...props }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <ReactStrapNavbar
      color={color}
      light
      expand="md"
      className={`custom-navbar navbar-right`}
    >
      <NavbarBrand>
        <Link href="/">
          <a href="/" className="brand">
            Pencil
          </a>
        </Link>
      </NavbarBrand>

      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav
          className="mr-auto justify-content-end"
          style={{ width: "90%" }}
          navbar
        >
          <NavItem>
            <NavLink>
              <Link href="/posts">
                <a href="/" className="white link">
                  Posts
                </a>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link href="/albums">
                <a href="/" className="white link">
                  Albums
                </a>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link href="/about">
                <a href="/" className="white link">
                  About
                </a>
              </Link>
            </NavLink>
          </NavItem>
        </Nav>
        <NavbarText>Simple Text</NavbarText>
      </Collapse>
    </ReactStrapNavbar>
  );
};

export default Navbar;
