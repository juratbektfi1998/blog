import React from 'react'
import { Button as ReactStrapButton } from 'reactstrap';
import omit from 'lodash.omit';

import IStyle from 'types/style';

export type TButtonProps = {
    text: string;
    outline?:boolean,
    style?:IStyle
}

const Button: React.FC<TButtonProps> = (props) => <ReactStrapButton {...omit<TButtonProps>(props, ['text'])}>{props.text}</ReactStrapButton>

export default Button
