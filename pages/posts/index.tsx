import { useEffect, useState } from 'react';
import { GetStaticPaths, GetStaticProps, InferGetStaticPropsType } from "next";
import { Container, Row, Col } from "reactstrap";
import PostList from "src/components/post_list";
import Button from 'src/components/button'
import BackButton from 'src/components/back_button';
import IPostsType from "types/posts";

import 'styles/Posts.module.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Posts(
    props: InferGetStaticPropsType<typeof getStaticProps>
) {

    const [isFade, setIsFade] = useState<boolean>(false);

    useEffect(() => {
        setIsFade(true)
    }, [])

    return (
        <Container fluid={true} className='posts-container'>
            <Row>
                <BackButton size={{ xl: '2' }} />
                <Col xl='10'>
                    <h1>All Posts</h1>
                </Col>
            </Row>
            {props['posts'].length !== 0 ? <PostList posts={props.posts} isFade={isFade} /> : <p>Empty no posts</p>}
        </Container>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {

    let initialValues = {
        props: {
            posts: []
        }
    }
    try {
        const res: any = await fetch(`${process.env.BASE_URL}/posts`);

        const posts: IPostsType = await res.json();

        if (!posts) return initialValues;

        initialValues['props']['posts'] = posts;

        return initialValues;
    } catch (e) {
        return initialValues;
    }
};
