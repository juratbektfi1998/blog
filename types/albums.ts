import IAlbum from './album';

type IAlbums=Array<IAlbum>;

export default IAlbums;