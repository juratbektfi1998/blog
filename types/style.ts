interface IStyle{
  [key:string]:string | number
}
export default IStyle;