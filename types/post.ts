interface IPostInterface{
    userId:number;
    id:number;
    title:string;
    body:string;
}

export default IPostInterface;